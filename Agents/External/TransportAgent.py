# -*- coding: utf-8 -*-
"""
filename: SimpleInfoAgent

Antes de ejecutar hay que añadir la raiz del proyecto a la variable PYTHONPATH

Agente que se registra como agente de hoteles y espera peticiones

@author: javier
"""
from AgentUtil.APIKeys import AMADEUS_KEY

__author__ = 'javier'

from multiprocessing import Process, Queue
import socket
import argparse
import json

from flask import Flask, request
from rdflib import Graph, Namespace, Literal, BNode
from rdflib.namespace import FOAF, RDF
from rdflib.collection import Collection
from amadeus import Flights



from AgentUtil.OntoNamespaces import ACL, DSO
from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.ACLMessages import build_message, send_message, get_message_properties
from AgentUtil.Agent import Agent
from AgentUtil.Logging import config_logger


# Definimos los parametros de la linea de comandos
parser = argparse.ArgumentParser()
parser.add_argument('--open', default='localhost', help="Define si el servidor esta abierto al exterior o no")
parser.add_argument('--port', type=int, help="Puerto de comunicacion del agente")
parser.add_argument('--dhost', default='localhost', help="Host del agente de directorio")
parser.add_argument('--dport', type=int, help="Puerto de comunicacion del agente de directorio")

# Logging
logger = config_logger(level=1)

# parsing de los parametros de la linea de comandos
args = parser.parse_args()

# Configuration stuff
if args.port is None:
    port = 9001
else:
    port = args.port

if args.open:
    hostname = args.open
else:
    hostname = socket.gethostname()

if args.dport is None:
    dport = 9000
else:
    dport = args.dport

if args.dhost is None:
    dhostname = socket.gethostname()
else:
    dhostname = args.dhost

# Flask stuff
app = Flask(__name__)

# Configuration constants and variables
agn = Namespace("http://www.agentes.org#")

# Contador de mensajes
mss_cnt = 0

# Datos del Agente
TransportAgent = Agent('AgenteTransporte',
                       agn.AgenteTransporte,
                  'http://%s:%d/comm' % (hostname, port),
                  'http://%s:%d/Stop' % (hostname, port))

# Directory agent address
DirectoryAgent = Agent('DirectoryAgent',
                       agn.Directory,
                       'http://%s:%d/Register' % (dhostname, dport),
                       'http://%s:%d/Stop' % (dhostname, dport))

# Global dsgraph triplestore
dsgraph = Graph()

# Cola de comunicacion entre procesos
cola1 = Queue()


def register_message():
    """
    Envia un mensaje de registro al servicio de registro
    usando una performativa Request y una accion Register del
    servicio de directorio

    :param gmess:
    :return:
    """

    logger.info('Nos registramos')

    global mss_cnt

    gmess = Graph()

    # Construimos el mensaje de registro
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[TransportAgent.name + '-Register']
    gmess.add((reg_obj, RDF.type, DSO.Register))
    gmess.add((reg_obj, DSO.Uri, TransportAgent.uri))
    gmess.add((reg_obj, FOAF.Name, Literal(TransportAgent.name)))
    gmess.add((reg_obj, DSO.Address, Literal(TransportAgent.address)))
    gmess.add((reg_obj, DSO.AgentType, DSO.TransportAgent))

    # Lo metemos en un envoltorio FIPA-ACL y lo enviamos
    gr = send_message(
        build_message(gmess, perf=ACL.request,
                      sender=TransportAgent.uri,
                      receiver=DirectoryAgent.uri,
                      content=reg_obj,
                      msgcnt=mss_cnt),
        DirectoryAgent.address)
    mss_cnt += 1

    return gr

def search_fly(aeropuerto_origen, aeropuerto_destino, fecha_ida, fecha_vuelta, numero_pasajeros, classe="ECONOMY", precio_maximo=10000):
    flights = Flights(AMADEUS_KEY)
    logger.info("Esperando respuesta de Amadeus...")
    r = flights.low_fare_search(
        origin=aeropuerto_origen,
        destination=aeropuerto_destino,
        departure_date=fecha_ida,
        return_date=fecha_vuelta,
        adults=numero_pasajeros,
        max_price=precio_maximo,
        travel_class=classe,
        nonstop=True)
    logger.info(r)
    # IMPORTANT -> r: La resposta d'amadeus, ja es un diccionari python.
    return r

def search_airport(ciudad):
    logger.info(ciudad)
    flights = Flights(AMADEUS_KEY)
    logger.info("Buscando aeropuerto en Amadeus...")
    r = flights.auto_complete(
        term=ciudad,
        country="ES")
    logger.info(r)
    if not r:
        return None
    return r[0]["value"]

def add_vuelo_a_grafo(vuelo, gr):
    '''
    :param vuelo: Diccionario que es una parte de la respues de Amadeus
    :param gr: Grafo rdf para crear el mensaje de respuesta.
    :return: Objecto vuelo creado y añadido al grafo
    '''
    # Para diferenciar los diferentes vuelos, creo un objecto con
    # el numero de la compañia y el numero de vuelo
    
    vuelo_obj = DSO["Vuelo" + vuelo["aircraft"] + vuelo["flight_number"]]
    gr.add((vuelo_obj, RDF.type, DSO.Vuelo))
    gr.add((vuelo_obj, DSO.NumeroVuelo, Literal(vuelo["flight_number"])))
    gr.add((vuelo_obj, DSO.AeropuertoOrigen, Literal(vuelo["origin"]["airport"])))
    gr.add((vuelo_obj, DSO.AeropuertoDestino, Literal(vuelo["destination"]["airport"])))
    gr.add((vuelo_obj, DSO.FechaSalida, Literal(vuelo["departs_at"])))
    gr.add((vuelo_obj, DSO.FechaLlegada, Literal(vuelo["arrives_at"])))
    gr.add((vuelo_obj, DSO.Tarifa, Literal(vuelo["booking_info"]["travel_class"])))

    return vuelo_obj

@app.route("/iface", methods=['GET', 'POST'])
def browser_iface():
    """
    Permite la comunicacion con el agente via un navegador
    via un formulario
    """
    return 'Nothing to see here'


@app.route("/Stop")
def stop():
    """
    Entrypoint que para el agente

    :return:
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"


@app.route("/comm")
def comunicacion():
    """
    Entrypoint de comunicacion del agente
    Simplemente retorna un objeto fijo que representa una
    respuesta a una busqueda de vuelo

    Asumimos que se reciben siempre acciones que se refieren a lo que puede hacer
    el agente (buscar con ciertas restricciones, reservar)
    Las acciones se mandan siempre con un Request
    Prodriamos resolver las busquedas usando una performativa de Query-ref
    
    """

    def process_fly_response(data):
        '''
        
        :param data: Diccionario que recibimos como respuesta de Amadeus
        :return: Build message
        '''
        gr = Graph()
        gr.bind('dso', DSO)
        # El concepto "padre" (rsp_obj) decimos que es BusquedaVuelos y decimos que tiene Itinerarios
        # porque es un contenedor de itinerarios
        rsp_obj = DSO.BusquedaVuelos
        gr.add((rsp_obj, RDF.type, DSO.ContenedorItinerarios))
        contenedorItinerarios = Collection(gr, rsp_obj)
        # Si no encontramos nada retornamos un inform sin contenido
        if 'status' in data and data['status'] == 400:
            return build_message(Graph(),
                                 ACL.inform,
                                 sender=TransportAgent.uri,
                                 msgcnt=mss_cnt)
        # Miramos la información de la API para ver que devuelve y procesar la información.
        for result in data["results"]:
            itinerarios = result["itineraries"]
            precio = result["fare"]["total_price"]
            for itinerario in itinerarios: #Solo hay vuelos directos
                itinerario_obj = BNode()
                vuelo_ida = itinerario["outbound"]["flights"][0]
                vuelo_vuelta = itinerario["inbound"]["flights"][0]
                vi = add_vuelo_a_grafo(vuelo_ida, gr)
                vv = add_vuelo_a_grafo(vuelo_vuelta, gr)
                gr.add((itinerario_obj, DSO.VueloIda, vi))
                gr.add((itinerario_obj, DSO.VueloVuelta, vv))
                gr.add((itinerario_obj, DSO.Precio, Literal(precio)))

                contenedorItinerarios.append(itinerario_obj)


            return build_message(gr,
                                 ACL.inform,
                                 sender=TransportAgent.uri,
                                 msgcnt=mss_cnt,
                                 receiver=msgdic['sender'],
                                 content=rsp_obj)
    def process_search():
        logger.info('Peticion de busqueda de vuelos')
        info = gm.value(subject=content, predicate=DSO.Utiliza)
        ciudad_origen = gm.value(subject=info, predicate=DSO.CiudadOrigen)
        ciudad_destino = gm.value(subject=info, predicate=DSO.CiudadDestino)

        aeropuerto_origen = search_airport(ciudad_origen)
        aeropuerto_destino = search_airport(ciudad_destino)
        if not aeropuerto_origen or not aeropuerto_destino:
            return build_message(Graph(),
                                 ACL.inform,
                                 sender=TransportAgent.uri,
                                 msgcnt=mss_cnt)

        fecha_ida = gm.value(subject=info, predicate=DSO.FechaSalida)
        fecha_vuelta = gm.value(subject=info, predicate=DSO.FechaVuelta)
        numero_pasajeros = gm.value(subject=info, predicate=DSO.NumeroPersonas)
        precio_maximo = gm.value(subject=info, predicate=DSO.PresupuestoVuelo)
        classe = gm.value(subject=info, predicate=DSO.ClaseVuelo)

        response = search_fly(aeropuerto_origen, aeropuerto_destino,
                              fecha_ida, fecha_vuelta, numero_pasajeros, classe, precio_maximo)
        if response:
            return process_fly_response(response)
        else:
            # Si no encontramos nada retornamos un inform sin contenido
            return build_message(Graph(),
                ACL.inform,
                sender=TransportAgent.uri,
                msgcnt=mss_cnt)


    global dsgraph
    global mss_cnt

    logger.info('Peticion de informacion recibida')

    # Extraemos el mensaje y creamos un grafo con el
    message = request.args['content']
    gm = Graph()
    gm.parse(data=message)

    msgdic = get_message_properties(gm)

    # Comprobamos que sea un mensaje FIPA ACL
    if msgdic is None:
        # Si no es, respondemos que no hemos entendido el mensaje
        gr = build_message(Graph(), ACL['not-understood'], sender=TransportAgent.uri, msgcnt=mss_cnt)
    else:
        # Obtenemos la performativa
        perf = msgdic['performative']

        if perf != ACL.request:
            # Si no es un request, respondemos que no hemos entendido el mensaje
            gr = build_message(Graph(), ACL['not-understood'], sender=TransportAgent.uri, msgcnt=mss_cnt)
        else:
            # Extraemos el objeto del contenido que ha de ser una accion de la ontologia de acciones del agente
            # de registro

            # Averiguamos el tipo de la accion
            accion = None
            if 'content' in msgdic:
                content = msgdic['content']
                accion = gm.value(subject=content, predicate=RDF.type)
            IAA = Namespace('IAActions')
            # Aqui realizariamos lo que pide la accion
            if accion == IAA.Search:
                gr = process_search()
            # No habia ninguna accion en el mensaje
            else:
                gr = build_message(Graph(),
                               ACL['not-understood'],
                               sender=TransportAgent.uri,
                               msgcnt=mss_cnt)


    mss_cnt += 1

    logger.info('Respondemos a la peticion')

    return gr.serialize(format='xml')


def tidyup():
    """
    Acciones previas a parar el agente

    """
    global cola1
    cola1.put(0)


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    # Registramos el agente
    gr = register_message()

    # Escuchando la cola hasta que llegue un 0
    fin = False
    while not fin:
        while cola.empty():
            pass
        v = cola.get()
        if v == 0:
            fin = True
        else:
            print (v)

            # Selfdestruct
            # requests.get(InfoAgent.stop)


if __name__ == '__main__':
    # Ponemos en marcha los behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Ponemos en marcha el servidor
    app.run(host=hostname, port=port)

    # Esperamos a que acaben los behaviors
    ab1.join()
    logger.info('The End')
