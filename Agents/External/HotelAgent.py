# -*- coding: utf-8 -*-
"""
filename: SimpleInfoAgent

Antes de ejecutar hay que añadir la raiz del proyecto a la variable PYTHONPATH

Agente que se registra como agente de hoteles y espera peticiones

@author: javier
"""
from ast import literal_eval

from AgentUtil.APIKeys import AMADEUS_KEY

__author__ = 'javier'

from multiprocessing import Process, Queue
import socket
import argparse
import json

from flask import Flask, request
from rdflib import Graph, Namespace, Literal, BNode
from rdflib.namespace import FOAF, RDF
from rdflib.collection import Collection
from amadeus import Hotels

from AgentUtil.OntoNamespaces import ACL, DSO
from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.ACLMessages import build_message, send_message, get_message_properties
from AgentUtil.Agent import Agent
from AgentUtil.Logging import config_logger

# Definimos los parametros de la linea de comandos
parser = argparse.ArgumentParser()
parser.add_argument('--open', default='localhost', help="Define si el servidor esta abierto al exterior o no")
parser.add_argument('--port', type=int, help="Puerto de comunicacion del agente")
parser.add_argument('--dhost', default='localhost', help="Host del agente de directorio")
parser.add_argument('--dport', type=int, help="Puerto de comunicacion del agente de directorio")

# Logging
logger = config_logger(level=1)

# parsing de los parametros de la linea de comandos
args = parser.parse_args()

# Configuration stuff
if args.port is None:
    port = 9005
else:
    port = args.port

if args.open:
    hostname = args.open
else:
    hostname = socket.gethostname()

if args.dport is None:
    dport = 9000
else:
    dport = args.dport

if args.dhost is None:
    dhostname = socket.gethostname()
else:
    dhostname = args.dhost

# Flask stuff
app = Flask(__name__)

# Configuration constants and variables
agn = Namespace("http://www.agentes.org#")

# Contador de mensajes
mss_cnt = 0

# Datos del Agente
HotelAgent = Agent('AgenteHotel',
                   agn.AgenteHotel,
                   'http://%s:%d/comm' % (hostname, port),
                   'http://%s:%d/Stop' % (hostname, port))

# Directory agent address
DirectoryAgent = Agent('DirectoryAgent',
                       agn.Directory,
                       'http://%s:%d/Register' % (dhostname, dport),
                       'http://%s:%d/Stop' % (dhostname, dport))

# Global dsgraph triplestore
dsgraph = Graph()

# Cola de comunicacion entre procesos
cola1 = Queue()

city_coordinates = {
    'Barcelona': {
        'latitude': 41.39,
        'longitude': 2.15
    },
    'Madrid': {
        'latitude': 40.416775,
        'longitude': -3.703790
    },
    'Merida': {
        'latitude': 38.919144,
        'longitude': -6.340805
    },
    'San Sebastian': {
        'latitude': 43.312691,
        'longitude': -1.993332
    },
    'Santiago Compostela': {
        'latitude': 42.878212,
        'longitude': -8.544844
    },
    'Bilbao': {
        'latitude': 43.262985,
        'longitude': -2.935013
    },
    'Sevilla': {
        'latitude': 37.392529,
        'longitude': -5.994072
    }
}


def register_message():
    """
    Envia un mensaje de registro al servicio de registro
    usando una performativa Request y una accion Register del
    servicio de directorio

    :param gmess:
    :return:
    """

    logger.info('Nos registramos')

    global mss_cnt

    gmess = Graph()

    # Construimos el mensaje de registro
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[HotelAgent.name + '-Register']
    gmess.add((reg_obj, RDF.type, DSO.Register))
    gmess.add((reg_obj, DSO.Uri, HotelAgent.uri))
    gmess.add((reg_obj, FOAF.Name, Literal(HotelAgent.name)))
    gmess.add((reg_obj, DSO.Address, Literal(HotelAgent.address)))
    gmess.add((reg_obj, DSO.AgentType, DSO.HotelAgent))

    # Lo metemos en un envoltorio FIPA-ACL y lo enviamos
    gr = send_message(
        build_message(gmess, perf=ACL.request,
                      sender=HotelAgent.uri,
                      receiver=DirectoryAgent.uri,
                      content=reg_obj,
                      msgcnt=mss_cnt),
        DirectoryAgent.address)
    mss_cnt += 1

    return gr


def search_coordinates(ciudad):
    coordinates = city_coordinates.get(ciudad)
    if not coordinates:
        return None, None
    return coordinates['latitude'], coordinates['longitude']


def search_hotel(latitud, longitud, radio, fecha_entrada, fecha_salida, precio_maximo=1000):
    hotels = Hotels(AMADEUS_KEY)
    logger.info("Esperando respuesta de Amadeus...")
    r = hotels.search_circle(
        latitude=latitud,
        longitude=longitud,
        radius=radio,
        check_in=fecha_entrada,
        check_out=fecha_salida,
        max_rate=precio_maximo,
        # number_of_results=1,
    )
    logger.info(r)
    # IMPORTANT -> r: La resposta d'amadeus, ja es un diccionari python.
    return r


def add_hotel_a_grafo(hotel, gr):
    '''
    :param vuelo: Diccionario que es una parte de la respues de Amadeus
    :param gr: Grafo rdf para crear el mensaje de respuesta.
    :return: Objecto hotel creado y añadido al grafo
    '''
    # Para diferenciar los diferentes vuelos, creo un objecto con
    # el numero de la compañia y el numero de vuelo

    hotel_obj = DSO["Hotel" + hotel["property_code"]]
    gr.add((hotel_obj, RDF.type, DSO.Hotel))
    gr.add((hotel_obj, DSO.NombreHotel, Literal(hotel["property_name"])))
    address = hotel["address"]
    direccion = "%s, %s, %s, %s" % (address["line1"], address["city"], address["postal_code"], address["country"])
    gr.add((hotel_obj, DSO.Direccion, Literal(direccion)))
    gr.add((hotel_obj, DSO.Precio, Literal(hotel["total_price"]["amount"])))

    return hotel_obj


@app.route("/iface", methods=['GET', 'POST'])
def browser_iface():
    """
    Permite la comunicacion con el agente via un navegador
    via un formulario
    """
    return 'Nothing to see here'


@app.route("/Stop")
def stop():
    """
    Entrypoint que para el agente

    :return:
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"


@app.route("/comm")
def comunicacion():
    """
    Entrypoint de comunicacion del agente
    Simplemente retorna un objeto fijo que representa una
    respuesta a una busqueda de vuelo

    Asumimos que se reciben siempre acciones que se refieren a lo que puede hacer
    el agente (buscar con ciertas restricciones, reservar)
    Las acciones se mandan siempre con un Request
    Prodriamos resolver las busquedas usando una performativa de Query-ref
    
    """

    def process_hotel_response(data):
        '''
        
        :param data: Diccionario que recibimos como respuesta de Amadeus
        :return: Build message
        '''
        gr = Graph()
        gr.bind('dso', DSO)
        # El concepto "padre" (rsp_obj) decimos que es BusquedaVuelos y decimos que tiene Itinerarios
        # porque es un contenedor de itinerarios
        rsp_obj = DSO.BusquedaHoteles
        gr.add((rsp_obj, RDF.type, DSO.ContenedorHoteles))
        contenedorHoteles = Collection(gr, rsp_obj)
        # Si no encontramos nada retornamos un inform sin contenido
        if not data.get("results"):
            return build_message(Graph(),
                                 ACL.inform,
                                 sender=HotelAgent.uri,
                                 msgcnt=mss_cnt)
        # Miramos la información de la API para ver que devuelve y procesar la información.
        for hotel in data["results"]:
            hotel_obj = add_hotel_a_grafo(hotel, gr)
            contenedorHoteles.append(hotel_obj)

            return build_message(gr,
                                 ACL.inform,
                                 sender=HotelAgent.uri,
                                 msgcnt=mss_cnt,
                                 receiver=msgdic['sender'],
                                 content=rsp_obj)

    def process_search():
        logger.info('Peticion de busqueda de hoteles')
        info = gm.value(subject=content, predicate=DSO.Utiliza)
        ciudad_destino = gm.value(subject=info, predicate=DSO.CiudadDestino)
        latitud, longitud = search_coordinates(str(ciudad_destino))
        if not latitud or not longitud:
            return build_message(Graph(),
                                 ACL.inform,
                                 sender=HotelAgent.uri,
                                 msgcnt=mss_cnt)
        alojamiento_centrico = gm.value(subject=info, predicate=DSO.AlojamientoCentrico)
        if str(alojamiento_centrico) == "True":
            radio = 5
        else:
            radio = 10

        fecha_entrada = gm.value(subject=info, predicate=DSO.FechaSalida)
        fecha_salida = gm.value(subject=info, predicate=DSO.FechaVuelta)
        precio_maximo = gm.value(subject=info, predicate=DSO.PresupuestoAlojamiento)
        response = search_hotel(latitud, longitud, radio, fecha_entrada, fecha_salida, precio_maximo)
        if response:
            return process_hotel_response(response)
        else:
            # Si no encontramos nada retornamos un inform sin contenido
            return build_message(Graph(),
                                 ACL.inform,
                                 sender=HotelAgent.uri,
                                 msgcnt=mss_cnt)

    global dsgraph
    global mss_cnt

    logger.info('Peticion de informacion recibida')

    # Extraemos el mensaje y creamos un grafo con el
    message = request.args['content']
    gm = Graph()
    gm.parse(data=message)

    msgdic = get_message_properties(gm)

    # Comprobamos que sea un mensaje FIPA ACL
    if msgdic is None:
        # Si no es, respondemos que no hemos entendido el mensaje
        gr = build_message(Graph(), ACL['not-understood'], sender=HotelAgent.uri, msgcnt=mss_cnt)
    else:
        # Obtenemos la performativa
        perf = msgdic['performative']

        if perf != ACL.request:
            # Si no es un request, respondemos que no hemos entendido el mensaje
            gr = build_message(Graph(), ACL['not-understood'], sender=HotelAgent.uri, msgcnt=mss_cnt)
        else:
            # Extraemos el objeto del contenido que ha de ser una accion de la ontologia de acciones del agente
            # de registro

            # Averiguamos el tipo de la accion
            accion = None
            if 'content' in msgdic:
                content = msgdic['content']
                accion = gm.value(subject=content, predicate=RDF.type)
            IAA = Namespace('IAActions')
            # Aqui realizariamos lo que pide la accion
            if accion == IAA.Search:
                gr = process_search()
            # No habia ninguna accion en el mensaje
            else:
                gr = build_message(Graph(),
                                   ACL['not-understood'],
                                   sender=HotelAgent.uri,
                                   msgcnt=mss_cnt)

    mss_cnt += 1

    logger.info('Respondemos a la peticion')

    return gr.serialize(format='xml')


def tidyup():
    """
    Acciones previas a parar el agente

    """
    global cola1
    cola1.put(0)


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    # Registramos el agente
    gr = register_message()

    # Escuchando la cola hasta que llegue un 0
    fin = False
    while not fin:
        while cola.empty():
            pass
        v = cola.get()
        if v == 0:
            fin = True
        else:
            print (v)

            # Selfdestruct
            # requests.get(InfoAgent.stop)


if __name__ == '__main__':
    # Ponemos en marcha los behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Ponemos en marcha el servidor
    app.run(host=hostname, port=port)

    # Esperamos a que acaben los behaviors
    ab1.join()
    logger.info('The End')
