__author__ = 'albert'

import argparse
import socket
from multiprocessing import Process, Queue

from flask import Flask, request, jsonify, Response
from rdflib import Graph, Namespace, Literal
from rdflib.namespace import FOAF, RDF

from AgentUtil.ACLMessages import build_message, send_message
from AgentUtil.Agent import Agent
from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.Logging import config_logger
from AgentUtil.OntoNamespaces import ACL, DSO

# bash parameters defined
parser = argparse.ArgumentParser()
parser.add_argument('--open', default='localhost', help="Define si el servidor esta abierto al exterior o no")
parser.add_argument('--port', type=int, help="Puerto de comunicacion del agente")
parser.add_argument('--dhost', default='localhost', help="Host del agente de directorio")
parser.add_argument('--dport', type=int, help="Puerto de comunicacion del agente de directorio")

# logging
logger = config_logger(level=1)

# parsing the bash parameters
args = parser.parse_args()

# configuration stuff
if args.port is None:
    port = 9007
else:
    port = args.port

if args.open:
    hostname = args.open
else:
    hostname = socket.gethostname()

if args.dport is None:
    dport = 9000
else:
    dport = args.dport

if args.dhost is None:
    dhostname = socket.gethostname()
else:
    dhostname = args.dhost

# Flask stuff
app = Flask(__name__)

# configuration constants and variables
agn = Namespace("http://www.agentes.org#")

# Supuesta ontologia de acciones de agentes de informacion
IAA = Namespace('IAActions')

# messages count
mss_cnt = 0

rsv_id = 0

# Global dsgraph triplestore
dsgraph = Graph()

# UserAgent info
ReservatorAgent = Agent('AgenteReservador',
                        agn.ReservatorAgent,
                        'http://%s:%d/comm' % (hostname, port),
                        'http://%s:%d/Stop' % (hostname, port))

# Directory agent address
DirectoryAgent = Agent('DirectoryAgent',
                       agn.Directory,
                       'http://%s:%d/Register' % (dhostname, dport),
                       'http://%s:%d/Stop' % (dhostname, dport))

# Global dsgraph triple store
dsgraph = Graph()

# Queue communication between process
cola1 = Queue()


def register_message():
    """
    Send a message to the register service using a request and a register action of the directory service

    :return:
    """

    logger.info('Nos registramos')
    global mss_cnt
    gmess = Graph()

    # building the register message
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[ReservatorAgent.name + '-Register']
    gmess.add((reg_obj, RDF.type, DSO.Register))
    gmess.add((reg_obj, DSO.Uri, ReservatorAgent.uri))
    gmess.add((reg_obj, FOAF.Name, Literal(ReservatorAgent.name)))
    gmess.add((reg_obj, DSO.Address, Literal(ReservatorAgent.address)))
    gmess.add((reg_obj, DSO.AgentType, DSO.AgenteReservador))

    # put the message into a FIPA-ACL and send it
    gr = send_message(
        build_message(gmess, perf=ACL.request,
                      sender=ReservatorAgent.uri,
                      receiver=DirectoryAgent.uri,
                      content=reg_obj,
                      msgcnt=mss_cnt),
        DirectoryAgent.address)
    mss_cnt += 1

    return gr


def directory_search_message(type):
    """
    Search a Agent of the type passed by parameter into the SimpleDirectoryService

    :param type:
    :return:
    """
    global mss_cnt
    logger.info('Buscamos en el servicio de registro')

    gmess = Graph()

    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[ReservatorAgent.name + '-search']
    gmess.add((reg_obj, RDF.type, DSO.Search))
    gmess.add((reg_obj, DSO.AgentType, type))

    msg = build_message(gmess, perf=ACL.request,
                        sender=ReservatorAgent.uri,
                        receiver=DirectoryAgent.uri,
                        content=reg_obj,
                        msgcnt=mss_cnt)
    gr = send_message(msg, DirectoryAgent.address)
    mss_cnt += 1
    logger.info('Recibimos informacion del agente')

    return gr


@app.route("/Stop")
def stop():
    """
    Entrypoint to stop the agent

    :return: a String
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"


@app.route("/comm")
def comunicacion():
    def process_reservation():
        logger.info("Creamos el grafo de la reserva")

        global rsv_id

        gmess = Graph()
        gmess.bind('dso', DSO)

        rsp_obj = DSO.Reserva
        gmess.add((rsp_obj, RDF.type, DSO.ReservaInfo))
        rsv_id += 1
        gmess.add((rsp_obj, DSO.IdReserva, Literal(rsv_id)))
        gmess.add((rsp_obj, DSO.Respuesta, Literal('Realizada')))

        logger.info("Grafo reserva realizado")

        return build_message(gmess,
                             ACL.inform,
                             sender=ReservatorAgent.uri,
                             msgcnt=mss_cnt,
                             receiver=msgdic['sender'],
                             content=rsp_obj)

    global dsgraph
    global mss_cnt

    logger.info('Peticion de informacion recibida')

    # Extraemos el mensaje y creamos un grafo con el
    message = request.args['content']
    gm = Graph()
    gm.parse(data=message)

    msgdic = get_message_properties(gm)

    # Comprobamos que sea un mensaje FIPA ACL
    if msgdic is None:
        # Si no es, respondemos que no hemos entendido el mensaje
        gr = build_message(Graph(), ACL['not-understood'], sender=ReservatorAgent.uri, msgcnt=mss_cnt)
    else:
        # Obtenemos la performativa
        perf = msgdic['performative']

        if perf != ACL.request:
            # Si no es un request, respondemos que no hemos entendido el mensaje
            gr = build_message(Graph(), ACL['not-understood'], sender=ReservatorAgent.uri, msgcnt=mss_cnt)
        else:
            # Extraemos el objeto del contenido que ha de ser una accion de la ontologia de acciones del agente
            # de registro

            # Averiguamos el tipo de la accion
            accion = None
            if 'content' in msgdic:
                content = msgdic['content']
                accion = gm.value(subject=content, predicate=RDF.type)
            # IAA = Namespace('IAActions')
            # Aqui realizariamos lo que pide la accion
            if accion == IAA.Reservar:
                gr = process_reservation()
            # No habia ninguna accion en el mensaje
            else:
                gr = build_message(Graph(),
                                   ACL['not-understood'],
                                   sender=ReservatorAgent.uri,
                                   msgcnt=mss_cnt)

            '''
            # Por ahora simplemente retornamos un Inform-done
            gr = build_message(fg,
                               ACL['inform-done'],
                               sender=TransportAgent.uri,
                               msgcnt=mss_cnt,
                               receiver=msgdic['sender'], )
            '''
    mss_cnt += 1

    logger.info('Respondemos a la peticion')

    return gr.serialize(format='xml')


def tidyup():
    """
    Acciones previas a parar el agente

    """
    global cola1
    cola1.put(0)


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    # Agent register
    gr = register_message()

    # Listening the queue until a 0 arrives
    fin = False
    while not fin:
        while cola.empty():
            pass
        v = cola.get()
        if v == 0:
            fin = True
        else:
            print (v)


def get_message_properties(msg):
    """
    Extrae las propiedades de un mensaje ACL como un diccionario.
    Del contenido solo saca el primer objeto al que apunta la propiedad

    Los elementos que no estan, no aparecen en el diccionario
    """
    props = {'performative': ACL.performative, 'sender': ACL.sender,
             'receiver': ACL.receiver, 'ontology': ACL.ontology,
             'conversation-id': ACL['conversation-id'],
             'in-reply-to': ACL['in-reply-to'], 'content': ACL.content}
    msgdic = {}  # Diccionario donde se guardan los elementos del mensaje

    # Extraemos la parte del FipaAclMessage del mensaje
    valid = msg.value(predicate=RDF.type, object=ACL.FipaAclMessage)

    # Extraemos las propiedades del mensaje
    if valid is not None:
        for key in props:
            val = msg.value(subject=valid, predicate=props[key])
            if val is not None:
                msgdic[key] = val
    return msgdic


if __name__ == '__main__':
    # Start the behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Starting server
    app.run(host=hostname, port=port)

    # Waiting behaviors end
    ab1.join()
    logger.info('The End')
