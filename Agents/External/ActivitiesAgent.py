# -*- coding: utf-8 -*-
"""
filename: SimpleInfoAgent

Antes de ejecutar hay que añadir la raiz del proyecto a la variable PYTHONPATH

Agente que se registra como agente de hoteles y espera peticiones

@author: javier
"""
from AgentUtil.APIKeys import GOOGLE_PLACES_KEY
__author__ = 'javier'

from multiprocessing import Process, Queue
import socket
import argparse
import json

from flask import Flask, request
from rdflib import Graph, Namespace, Literal
from rdflib.namespace import FOAF, RDF
from rdflib.collection import Collection

from googleplaces import GooglePlaces, types, lang

from AgentUtil.OntoNamespaces import ACL, DSO
from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.ACLMessages import build_message, send_message, get_message_properties
from AgentUtil.Agent import Agent
from AgentUtil.Logging import config_logger

from pymongo import MongoClient

# Definimos los parametros de la linea de comandos
parser = argparse.ArgumentParser()
parser.add_argument('--open', default='localhost', help="Define si el servidor esta abierto al exterior o no")
parser.add_argument('--port', type=int, help="Puerto de comunicacion del agente")
parser.add_argument('--dhost', default='localhost', help="Host del agente de directorio")
parser.add_argument('--dport', type=int, help="Puerto de comunicacion del agente de directorio")

#conectamos a la BD
try:
    client = MongoClient()
    db = client.fibiatge
    activitiesCollection = db.activities
except:
    pass

# Logging
logger = config_logger(level=1)

# parsing de los parametros de la linea de comandos
args = parser.parse_args()

# Configuration stuff
if args.port is None:
    port = 9006
else:
    port = args.port

if args.open:
    hostname = args.open
else:
    hostname = socket.gethostname()

if args.dport is None:
    dport = 9000
else:
    dport = args.dport

if args.dhost is None:
    dhostname = socket.gethostname()
else:
    dhostname = args.dhost

# Flask stuff
app = Flask(__name__)

# Configuration constants and variables
agn = Namespace("http://www.agentes.org#")

# Contador de mensajes
mss_cnt = 0

# Datos del Agente
ActivitiesAgent = Agent('AgenteActividades',
                       agn.AgenteActividades,
                  'http://%s:%d/comm' % (hostname, port),
                  'http://%s:%d/Stop' % (hostname, port))

# Directory agent address
DirectoryAgent = Agent('DirectoryAgent',
                       agn.Directory,
                       'http://%s:%d/Register' % (dhostname, dport),
                       'http://%s:%d/Stop' % (dhostname, dport))

# Global dsgraph triplestore
dsgraph = Graph()

# Cola de comunicacion entre procesos
cola1 = Queue()


def register_message():
    """
    Envia un mensaje de registro al servicio de registro
    usando una performativa Request y una accion Register del
    servicio de directorio

    :param gmess:
    :return:
    """

    logger.info('Peticion de registro del Agente Actividades')

    global mss_cnt

    gmess = Graph()

    # Construimos el mensaje de registro
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[ActivitiesAgent.name + '-Register']
    gmess.add((reg_obj, RDF.type, DSO.Register))
    gmess.add((reg_obj, DSO.Uri, ActivitiesAgent.uri))
    gmess.add((reg_obj, FOAF.Name, Literal(ActivitiesAgent.name)))
    gmess.add((reg_obj, DSO.Address, Literal(ActivitiesAgent.address)))
    gmess.add((reg_obj, DSO.AgentType, DSO.ActivitiesAgent))

    # Lo metemos en un envoltorio FIPA-ACL y lo enviamos
    gr = send_message(
        build_message(gmess, perf=ACL.request,
                      sender=ActivitiesAgent.uri,
                      receiver=DirectoryAgent.uri,
                      content=reg_obj,
                      msgcnt=mss_cnt),
        DirectoryAgent.address)
    mss_cnt += 1

    return gr


def search_activities(pais, ciudad, tipo, radio):
    """
    Permite la comunicacion con la api de Google places 
    :param pais: Pais donde se quierne buscar las actividades
    :param ciudad: Ciudad del pais donde se quieren buscar las actividades
    :param palabras_clabe: Palabras clabe sobre las actividades que se quieren buscar
    :param radio: Radio de distancia para encontrar las actividades
    :return: Lista de objetos con las actividades
    """
    google_places = GooglePlaces(GOOGLE_PLACES_KEY)
    logger.info("Buscando los places de tipo "+tipo)
    query_result = google_places.nearby_search(
        location=ciudad + ', ' + pais,
        types=[tipo],
        radius=radio)

    return query_result


def add_actividad_a_grafo(actividad, gr):
    '''
    Permite añadir una actividad a un grafo rdf
    :param actividad: Diccionario que es una parte de la respues de Google 
    :param gr: Grafo rdf para crear el mensaje de respuesta.
    :return: Objecto actividad creado y añadido al grafo
    '''
    # Para diferenciar los diferentes vuelos, creo un objecto con
    # el numero de la compañia y el numero de vuelo
    
    actividad_obj = DSO["Actividad" + actividad["id"]]
    gr.add((actividad_obj, RDF.type, DSO.Actividad))
    gr.add((actividad_obj, DSO.Nombre, Literal(actividad["name"])))
    gr.add((actividad_obj, DSO.Direccion, Literal(actividad["address"])))
    logger.info(actividad)
    gr.add((actividad_obj, DSO.FranjaHoraria, Literal(actividad["hours"])))

    #La tarifa la pongo como un literal (en la Ontologia teniamos definido otro Concepto)
    return actividad_obj


@app.route("/iface", methods=['GET', 'POST'])
def browser_iface():
    """
    Permite la comunicacion con el agente via un navegador
    via un formulario
    """
    return 'Nothing to see here'


@app.route("/Stop")
def stop():
    """
    Entrypoint que para el agente

    :return:
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"


@app.route("/comm")
def comunicacion():
    """
    Entrypoint de comunicacion del agente
    Simplemente retorna un objeto fijo que representa una
    respuesta a una busqueda de vuelo

    Asumimos que se reciben siempre acciones que se refieren a lo que puede hacer
    el agente (buscar con ciertas restricciones, reservar)
    Las acciones se mandan siempre con un Request
    Prodriamos resolver las busquedas usando una performativa de Query-ref
    
    """
    def process_activities_response(query_result):
        '''
        :param query_result: Diccionario que recibimos como respuesta de Google
        :return: Build message
        '''
        gr = Graph()
        gr.bind('dso', DSO)
        rsp_obj = DSO.BusquedaActividades
        gr.add((rsp_obj, RDF.type, DSO.ContenedorActividades))
        contenedor_actividades = Collection(gr, rsp_obj)

        if query_result.has_attributions:
            print query_result.html_attributions
        for place in query_result.places:
            place.get_details()
            a = add_actividad_a_grafo(place, gr)
            contenedor_actividades.append(a)

        return build_message(gr, ACL.inform, sender=ActivitiesAgent.uri, msgcnt=mss_cnt, receiver=msgdic['sender'],
                             content=rsp_obj)

    def process_search():
        logger.info('Peticion de busqueda de sitios')
        info = gm.value(subject=content, predicate=DSO.Utiliza)
        pais = "spain"
        ciudad = gm.value(subject=info, predicate=DSO.CiudadDestino)
        tipos = gm.value(subject=info, predicate=DSO.TematicaActividad)
        radio = 10000
        #precio_maximo = gm.value(subject=content, predicate=DSO.maxprice)
        #logger.info(tipos)
        activities_arr = []

        gr = Graph()
        gr.bind('dso', DSO)
        rsp_obj = DSO.BusquedaActividades
        gr.add((rsp_obj, RDF.type, DSO.ContenedorActividades))
        contenedor_actividades = Collection(gr, rsp_obj)

        types_list = []
        if str(tipos) == 'cultura':
            types_list = [types.TYPE_MUSEUM, types.TYPE_MOVIE_THEATER, types.TYPE_ART_GALLERY]
        if str(tipos) == 'aventura':
            types_list = [types.TYPE_ZOO, types.TYPE_AQUARIUM, types.TYPE_CASINO]
        if str(tipos) == 'gastronomia':
            types_list = [types.TYPE_BAR, types.TYPE_RESTAURANT, types.TYPE_CAFE]

        try:
            actividadesGuardadas = activitiesCollection.find_one({"pais": pais, "ciudad": ciudad, "tipos": tipos})
        except:
            actividadesGuardadas = None
            pass
        if actividadesGuardadas == None:
            resCont = 0
            for type in types_list:
                while True:
                    try:
                        response = search_activities(pais, ciudad, type, radio)
                        break
                    except Exception, e:
                        logger.error(str(e))
                        pass
                # Si hi ha resposta es processa i s'afegeix al graf
                if response:
                    if response.has_attributions:
                        print response.html_attributions
                    for place in response.places:
                        place.get_details()
                        placeToStore = {
                            "id": place.place_id,
                            "name": place.details["name"],
                            "address": place.details["formatted_address"],
                            "hours": json.dumps(place.details.get("opening_hours"), ensure_ascii=False)
                        }
                        a = add_actividad_a_grafo(placeToStore, gr)
                        contenedor_actividades.append(a)
                        activities_arr.append(placeToStore)
                else:
                    ++resCont

            if resCont == len(types_list):
                # Si no encontramos nada retornamos un inform sin contenido
                return build_message(Graph(), ACL.inform, sender=ActivitiesAgent.uri, msgcnt=mss_cnt)
            else:
                # guardamos a la bd
                activities = {
                    "pais": pais,
                    "ciudad": ciudad,
                    "tipos": tipos,
                    "activities": activities_arr
                }
                try:
                    activitiesCollection.insert_one(activities)
                except:
                    pass
                return build_message(gr, ACL.inform, sender=ActivitiesAgent.uri, msgcnt=mss_cnt, receiver=msgdic['sender'],
                                 content=rsp_obj)
        else:
            for place in actividadesGuardadas["activities"]:
                a = add_actividad_a_grafo(place, gr)
                contenedor_actividades.append(a)

            return build_message(gr, ACL.inform, sender=ActivitiesAgent.uri, msgcnt=mss_cnt, receiver=msgdic['sender'],
                                 content=rsp_obj)

    global dsgraph
    global mss_cnt

    logger.info('Peticion de informacion recibida')

    # Extraemos el mensaje y creamos un grafo con el
    message = request.args['content']
    gm = Graph()
    gm.parse(data=message)

    msgdic = get_message_properties(gm)

    # Comprobamos que sea un mensaje FIPA ACL
    if msgdic is None:
        # Si no es, respondemos que no hemos entendido el mensaje
        gr = build_message(Graph(), ACL['not-understood'], sender=ActivitiesAgent.uri, msgcnt=mss_cnt)
    else:
        # Obtenemos la performativa
        perf = msgdic['performative']

        if perf != ACL.request:
            # Si no es un request, respondemos que no hemos entendido el mensaje
            gr = build_message(Graph(), ACL['not-understood'], sender=ActivitiesAgent.uri, msgcnt=mss_cnt)
        else:
            # Extraemos el objeto del contenido que ha de ser una accion de la ontologia de acciones del agente
            # de registro

            # Averiguamos el tipo de la accion
            accion = None
            if 'content' in msgdic:
                content = msgdic['content']
                accion = gm.value(subject=content, predicate=RDF.type)
            IAA = Namespace('IAActions')
            # Aqui realizariamos lo que pide la accion
            if accion == IAA.Search:
                gr = process_search()
            # No habia ninguna accion en el mensaje
            else:
                gr = build_message(Graph(),
                               ACL['not-understood'],
                               sender=ActivitiesAgent.uri,
                               msgcnt=mss_cnt)


            '''
            # Por ahora simplemente retornamos un Inform-done
            gr = build_message(fg,
                               ACL['inform-done'],
                               sender=TransportAgent.uri,
                               msgcnt=mss_cnt,
                               receiver=msgdic['sender'], )
            '''
    mss_cnt += 1

    logger.info('Respondemos a la peticion')

    return gr.serialize(format='xml')


def tidyup():
    """
    Acciones previas a parar el agente

    """
    global cola1
    cola1.put(0)


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    # Registramos el agente
    gr = register_message()

    # Escuchando la cola hasta que llegue un 0
    fin = False
    while not fin:
        while cola.empty():
            pass
        v = cola.get()
        if v == 0:
            fin = True
        else:
            print (v)

            # Selfdestruct
            # requests.get(InfoAgent.stop)


if __name__ == '__main__':
    # Ponemos en marcha los behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Ponemos en marcha el servidor
    app.run(host=hostname, port=port)

    # Esperamos a que acaben los behaviors
    ab1.join()
    logger.info('The End')
