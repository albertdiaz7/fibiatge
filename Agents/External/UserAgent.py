__author__ = 'albert'

import argparse
import socket
import pickle
from multiprocessing import Process, Queue

import requests
from flask import Flask, render_template, request, json, jsonify, Response
from rdflib import Graph, Namespace, Literal
from rdflib.namespace import FOAF, RDF

from pymongo import MongoClient
from bson.objectid import ObjectId

from AgentUtil.ACLMessages import build_message, send_message
from AgentUtil.Agent import Agent
from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.Logging import config_logger
from AgentUtil.OntoNamespaces import ACL, DSO
from rdflib.collection import Collection

# bash parameters defined
parser = argparse.ArgumentParser()
parser.add_argument('--open', default='localhost', help="Define si el servidor esta abierto al exterior o no")
parser.add_argument('--port', type=int, help="Puerto de comunicacion del agente")
parser.add_argument('--dhost', default='localhost', help="Host del agente de directorio")
parser.add_argument('--dport', type=int, help="Puerto de comunicacion del agente de directorio")

# logging
logger = config_logger(level=1)

#conectamos a la BD
try:
    client = MongoClient()
    db = client.fibiatge
    planCollection = db.plans
except:
    pass

# parsing the bash parameters
args = parser.parse_args()

# configuration stuff
if args.port is None:
    port = 9004
else:
    port = args.port

if args.open:
    hostname = args.open
else:
    hostname = socket.gethostname()

if args.dport is None:
    dport = 9000
else:
    dport = args.dport

if args.dhost is None:
    dhostname = socket.gethostname()
else:
    dhostname = args.dhost

# Flask stuff
app = Flask(__name__)

# configuration constants and variables
agn = Namespace("http://www.agentes.org#")

# messages count
mss_cnt = 0

# UserAgent info
UserAgent = Agent('AgenteUsuario',
                  agn.UserAgent,
                  'http://%s:%d/comm' % (hostname, port),
                  'http://%s:%d/Stop' % (hostname, port))

# Directory agent address
DirectoryAgent = Agent('DirectoryAgent',
                       agn.Directory,
                       'http://%s:%d/Register' % (dhostname, dport),
                       'http://%s:%d/Stop' % (dhostname, dport))

# Global dsgraph triple store
dsgraph = Graph()

# Queue communication between process
cola1 = Queue()


def register_message():
    """
    Send a message to the register service using a request and a register action of the directory service

    :return:
    """

    logger.info('Nos registramos')
    global mss_cnt
    gmess = Graph()

    # building the register message
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[UserAgent.name + '-Register']
    gmess.add((reg_obj, RDF.type, DSO.Register))
    gmess.add((reg_obj, DSO.Uri, UserAgent.uri))
    gmess.add((reg_obj, FOAF.Name, Literal(UserAgent.name)))
    gmess.add((reg_obj, DSO.Address, Literal(UserAgent.address)))
    gmess.add((reg_obj, DSO.AgentType, DSO.UserAgent))

    # put the message into a FIPA-ACL and send it
    gr = send_message(
        build_message(gmess, perf=ACL.request,
                      sender=UserAgent.uri,
                      receiver=DirectoryAgent.uri,
                      content=reg_obj,
                      msgcnt=mss_cnt),
        DirectoryAgent.address)
    mss_cnt += 1

    return gr


def directory_search_message(type):
    """
    Search a Agent of the type passed by parameter into the SimpleDirectoryService

    :param type:
    :return:
    """
    global mss_cnt
    logger.info('Buscamos en el servicio de registro')

    gmess = Graph()

    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[UserAgent.name + '-search']
    gmess.add((reg_obj, RDF.type, DSO.Search))
    gmess.add((reg_obj, DSO.AgentType, type))

    msg = build_message(gmess, perf=ACL.request,
                        sender=UserAgent.uri,
                        receiver=DirectoryAgent.uri,
                        content=reg_obj,
                        msgcnt=mss_cnt)
    gr = send_message(msg, DirectoryAgent.address)
    mss_cnt += 1
    logger.info('Recibimos informacion del agente')

    return gr


def process_transport_response(plan_viaje, gr):
    itinerario_obj = gr.value(subject=plan_viaje, predicate=DSO.Itinerario)
    precio_itinerario = gr.value(subject=itinerario_obj, predicate=DSO.Precio)

    # vuelo de ida
    vuelo_ida = gr.value(subject=itinerario_obj, predicate=DSO.VueloIda)
    numero_vuelo_ida = gr.value(subject=vuelo_ida, predicate=DSO.NumeroVuelo)
    aeropuerto_origen_vuelo_ida = gr.value(subject=vuelo_ida, predicate=DSO.AeropuertoOrigen)
    aeropuerto_destino_vuelo_ida = gr.value(subject=vuelo_ida, predicate=DSO.AeropuertoDestino)
    fecha_salida_vuelo_ida = gr.value(subject=vuelo_ida, predicate=DSO.FechaSalida)
    fecha_llegada_vuelo_ida = gr.value(subject=vuelo_ida, predicate=DSO.FechaLlegada)
    tarifa_vuelo_ida = gr.value(subject=vuelo_ida, predicate=DSO.Tarifa)

    # vuelo de vuelta
    vuelo_vuelta = gr.value(subject=itinerario_obj, predicate=DSO.VueloVuelta)
    numero_vuelo_vuelta = gr.value(subject=vuelo_vuelta, predicate=DSO.NumeroVuelo)
    aeropuerto_origen_vuelo_vuelta = gr.value(subject=vuelo_vuelta, predicate=DSO.AeropuertoOrigen)
    aeropuerto_destino_vuelo_vuelta = gr.value(subject=vuelo_vuelta, predicate=DSO.AeropuertoDestino)
    fecha_salida_vuelo_vuelta = gr.value(subject=vuelo_vuelta, predicate=DSO.FechaSalida)
    fecha_llegada_vuelo_vuelta = gr.value(subject=vuelo_vuelta, predicate=DSO.FechaLlegada)
    tarifa_vuelo_vuelta = gr.value(subject=vuelo_vuelta, predicate=DSO.Tarifa)

    itinerario = {
        'vuelo_ida': {
            'numero_vuelo': numero_vuelo_ida,
            'aeropuerto_origen': aeropuerto_origen_vuelo_ida,
            'fecha_salida': fecha_salida_vuelo_ida,
            'aeropuerto_destino': aeropuerto_destino_vuelo_ida,
            'fecha_llegada': fecha_llegada_vuelo_ida,
            'tarifa': tarifa_vuelo_ida
        },
        'vuelo_vuelta': {
            'numero_vuelo': numero_vuelo_vuelta,
            'aeropuerto_origen': aeropuerto_origen_vuelo_vuelta,
            'fecha_salida': fecha_salida_vuelo_vuelta,
            'aeropuerto_destino': aeropuerto_destino_vuelo_vuelta,
            'fecha_llegada': fecha_llegada_vuelo_vuelta,
            'tarifa': tarifa_vuelo_vuelta
        },
        "precio": precio_itinerario
    }
    return itinerario


def process_hotel_response(plan_viaje, gr):
    hotel_obj = gr.value(subject=plan_viaje, predicate=DSO.Hotel)
    nombre = gr.value(subject=hotel_obj, predicate=DSO.NombreHotel)
    direccion = gr.value(subject=hotel_obj, predicate=DSO.Direccion)
    precio = gr.value(subject=hotel_obj, predicate=DSO.Precio)

    hotel = {
        'nombre': nombre,
        'direccion': direccion,
        'precio': precio
    }

    return hotel


def process_activities_response(plan_viaje, gr):
    logger.info("Processar resposta activitats")
    activities = []
    actividades = gr.value(subject=plan_viaje, predicate=DSO.Actividades)
    contenedorActividades = Collection(gr, actividades)
    for act in contenedorActividades:
        nombre = gr.value(subject=act, predicate=DSO.Nombre)
        direccion = gr.value(subject=act, predicate=DSO.Direccion)
        franja_horaria = gr.value(subject=act, predicate=DSO.FranjaHoraria)

        actividad = {"nombre": nombre,
                     "direccion":direccion,
                     "franja_horaria": franja_horaria}
        activities.append(actividad)

    return activities

def get_plan(id):
    return planCollection.find_one({"_id": ObjectId(id)})


@app.route("/api/trip", methods=['POST'])
def api_trip():
    """
    Agent communication via API
    """
    logger.info("Nueva peticion de plan de viaje")
    data = request.get_json()
    # trip info
    city_origin = Literal(data['city_origin'])
    departure_date = Literal(data['departure_date'])
    city_destiny = Literal(data['city_destiny'])
    return_date = Literal(data['return_date'])
    persons_number = Literal(data['persons_number'])

    # accommodation info
    rooms_number = Literal(data['rooms_number'])
    accommodation_center = Literal(data['accommodation_center'])
    hotel_budget = Literal(data['hotel_budget'])

    # flight info
    flight_class = Literal(data['flight_class'])
    flight_budget = Literal(data['flight_budget'])

    # activities info
    activity_theme = Literal(data['activity_theme'])

    # Searching the Generator Agent in the directory
    gr = directory_search_message(DSO.GeneratorAgent)

    # Obtaining the Agent direction of the response
    msg = gr.value(predicate=RDF.type, object=ACL.FipaAclMessage)
    content = gr.value(subject=msg, predicate=ACL.content)
    ragn_addr = gr.value(subject=content, predicate=DSO.Address)
    ragn_uri = gr.value(subject=content, predicate=DSO.Uri)

    """
    Send a action to GeneratorAgent
    """
    global mss_cnt
    logger.info('Preparamos un accion al Generator Agent')

    # Supuesta ontologia de acciones de agentes de informacion
    IAA = Namespace('IAActions')

    # graph with the action ontology
    gmess = Graph()
    gmess.bind('foaf', FOAF)
    gmess.bind('iaa', IAA)

    # concept EnviarInfoUsuario
    reg_obj = DSO.InfoUsuario
    gmess.add((reg_obj, RDF.type, IAA.Search))

    # fill the graph with the data info
    gmess.add((reg_obj, DSO.CiudadOrigen, city_origin))
    gmess.add((reg_obj, DSO.CiudadDestino, city_destiny))
    gmess.add((reg_obj, DSO.FechaSalida, departure_date))
    gmess.add((reg_obj, DSO.FechaVuelta, return_date))
    gmess.add((reg_obj, DSO.NumeroPersonas, persons_number))
    gmess.add((reg_obj, DSO.NumeroHabitaciones, rooms_number))
    gmess.add((reg_obj, DSO.AlojamientoCentrico, accommodation_center))
    gmess.add((reg_obj, DSO.PresupuestoAlojamiento, hotel_budget))
    gmess.add((reg_obj, DSO.ClaseVuelo, flight_class))
    gmess.add((reg_obj, DSO.PresupuestoVuelo, flight_budget))
    gmess.add((reg_obj, DSO.TematicaActividad, activity_theme))

    # send the message
    msg = build_message(gmess, perf=ACL.request,
                        sender=UserAgent.uri,
                        receiver=ragn_uri,
                        msgcnt=mss_cnt,
                        content=reg_obj)

    logger.info('Enviamos un accion al Generator Agent')

    gr = send_message(msg, ragn_addr)

    logger.info('Recibimos respuesta a la peticion al GeneratorAgent')

    # In gr there are a subject of ContenedorPlanViaje type that is the concept of Plan Viaje
    plan_viaje = gr.value(predicate=RDF.type, object=DSO.PlanViaje)
    num_viaje = gr.value(subject=plan_viaje, predicate=DSO.NumViaje)
    # print(gr.serialize(format="xml"))
    itinerario = process_transport_response(plan_viaje, gr)
    hotel = process_hotel_response(plan_viaje, gr)
    activities = process_activities_response(plan_viaje, gr)
    precio_hotel = hotel["precio"] if hotel.get("precio") else 0
    precio_itinerario = itinerario["precio"] if itinerario.get("precio") else 0

    # create the ordered dictionary that contains the PlanViaje to return to the user
    response_dict = {
        'trip_id': num_viaje,
        'itinerario': itinerario,
        'hotel': hotel,
        'actividades': activities,
        'pago': {
            'precio': float(precio_itinerario) + float(precio_hotel),
            'metodo': 'TARJETA'
        }
    }
    # jsonify the ordered dictionary that will be send it to the user
    response = jsonify(response_dict)

    mss_cnt += 1

    return response, 200


@app.route("/api/trip/<trip_id>/reservation", methods=['POST'])
def api_trip_reservation(trip_id):
    global mss_cnt

    #plan = get_plan(trip_id)
    #print (plan)
    #gr1 = Graph()
    #gr1.parse(data=plan)

    data = request.get_json()
    numero_tarjeta = data['numero_tarjeta']

    print ("Buscamos al Agente Reservador")

    # Searching the Generator Agent in the directory
    gr = directory_search_message(DSO.AgenteReservador)

    # Obtaining the Agent direction of the response
    msg = gr.value(predicate=RDF.type, object=ACL.FipaAclMessage)
    content = gr.value(subject=msg, predicate=ACL.content)
    ragn_addr = gr.value(subject=content, predicate=DSO.Address)
    ragn_uri = gr.value(subject=content, predicate=DSO.Uri)

    print (ragn_addr)

    print (ragn_uri)

    print ("Preparamos peticion al Agente Reservador")

    # Supuesta ontologia de acciones de agentes de informacion
    IAA = Namespace('IAActions')

    # graph with the action ontology
    gmess = Graph()
    gmess.bind('foaf', FOAF)
    gmess.bind('iaa', IAA)

    # concept EnviarInfoUsuario
    reg_obj = DSO.InfoUsuario
    gmess.add((reg_obj, RDF.type, IAA.Reservar))
    gmess.add((reg_obj, DSO.IdPlanViaje, Literal(trip_id)))

    # send the message
    msg = build_message(gmess, perf=ACL.request,
                        sender=UserAgent.uri,
                        receiver=ragn_uri,
                        msgcnt=mss_cnt,
                        content=reg_obj)

    print ("Enviamos peticion al Agente Reservador")

    gr = send_message(msg, ragn_addr)

    print ("Recibimos respuesta del Agente Reservador")

    print gr.serialize(format='xml')

    reserva = gr.value(predicate=RDF.type, object=DSO.ReservaInfo)
    respuesta = gr.value(subject=reserva, predicate=DSO.Respuesta)
    reserva_id = gr.value(subject=reserva, predicate=DSO.IdReserva)

    response_dict = {
        "id_plan_viaje": trip_id,
        "id_reserva:": reserva_id,
        "numero_tarjeta": numero_tarjeta,
        "confirmacion_reserva": respuesta
    }

    response = jsonify(response_dict)

    mss_cnt += 1

    return response, 200


@app.route("/Stop")
def stop():
    """
    Entrypoint to stop the agent

    :return: a String
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"


@app.route("/comm")
def comunicacion():
    """
    Entrypoint of communication
    """
    return "Hola"


def tidyup():
    """
    Acciones previas a parar el agente

    """
    global cola1
    cola1.put(0)


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    # Agent register
    gr = register_message()

    # Listening the queue until a 0 arrives
    fin = False
    while not fin:
        while cola.empty():
            pass
        v = cola.get()
        if v == 0:
            fin = True
        else:
            print (v)


if __name__ == '__main__':
    # Start the behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Starting server
    app.run(host=hostname, port=port)

    # Waiting behaviors end
    ab1.join()
    logger.info('The End')
