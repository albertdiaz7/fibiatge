# -*- coding: utf-8 -*-
"""
filename: SimpleInfoAgent

Antes de ejecutar hay que añadir la raiz del proyecto a la variable PYTHONPATH

Agente que se registra como agente de hoteles y espera peticiones

@author: javier
"""
from AgentUtil.APIKeys import AMADEUS_KEY

__author__ = 'javier'

from multiprocessing import Process, Queue, Manager
import socket
import argparse
import json

from flask import Flask, request
from rdflib import Graph, Namespace, Literal
from rdflib.namespace import FOAF, RDF
from rdflib.collection import Collection
from amadeus import Flights

from pymongo import MongoClient

from AgentUtil.OntoNamespaces import ACL, DSO
from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.ACLMessages import build_message, send_message, get_message_properties
from AgentUtil.Agent import Agent
from AgentUtil.Logging import config_logger

# Definimos los parametros de la linea de comandos
parser = argparse.ArgumentParser()
parser.add_argument('--open', default='localhost', help="Define si el servidor esta abierto al exterior o no")
parser.add_argument('--port', type=int, help="Puerto de comunicacion del agente")
parser.add_argument('--dhost', default='localhost', help="Host del agente de directorio")
parser.add_argument('--dport', type=int, help="Puerto de comunicacion del agente de directorio")

#conectamos a la BD
try:
    client = MongoClient()
    db = client.fibiatge
    planCollection = db.plans
except:
    pass

# Logging
logger = config_logger(level=1)

# parsing de los parametros de la linea de comandos
args = parser.parse_args()

# Configuration stuff
if args.port is None:
    port = 9003
else:
    port = args.port

if args.open:
    hostname = args.open
else:
    hostname = socket.gethostname()

if args.dport is None:
    dport = 9000
else:
    dport = args.dport

if args.dhost is None:
    dhostname = socket.gethostname()
else:
    dhostname = args.dhost

# Flask stuff
app = Flask(__name__)

# Configuration constants and variables
agn = Namespace("http://www.agentes.org#")
# Supuesta ontologia de acciones de agentes de informacion
IAA = Namespace('IAActions')
# Contador de mensajes
mss_cnt = 0

# Datos del Agente
AgenteGenerador = Agent('AgenteGenerador',
                        agn.AgenteGenerador,
                        'http://%s:%d/comm' % (hostname, port),
                        'http://%s:%d/Stop' % (hostname, port))

# Directory agent address
DirectoryAgent = Agent('DirectoryAgent',
                       agn.Directory,
                       'http://%s:%d/Register' % (dhostname, dport),
                       'http://%s:%d/Stop' % (dhostname, dport))

# Global dsgraph triplestore
dsgraph = Graph()

# Cola de comunicacion entre procesos
cola1 = Queue()

# Serial de numero de petición de plan de viaje para hacer pruebas
NUM_VIAJE = 1

PARALLEL = True


def register_message():
    """
    Envia un mensaje de registro al servicio de registro
    usando una performativa Request y una accion Register del
    servicio de directorio

    :param gmess:
    :return:
    """

    logger.info('Nos registramos')

    global mss_cnt

    gmess = Graph()

    # Construimos el mensaje de registro
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[AgenteGenerador.name + '-Register']
    gmess.add((reg_obj, RDF.type, DSO.Register))
    gmess.add((reg_obj, DSO.Uri, AgenteGenerador.uri))
    gmess.add((reg_obj, FOAF.Name, Literal(AgenteGenerador.name)))
    gmess.add((reg_obj, DSO.Address, Literal(AgenteGenerador.address)))
    gmess.add((reg_obj, DSO.AgentType, DSO.GeneratorAgent))

    # Lo metemos en un envoltorio FIPA-ACL y lo enviamos
    gr = send_message(
        build_message(gmess, perf=ACL.request,
                      sender=AgenteGenerador.uri,
                      receiver=DirectoryAgent.uri,
                      content=reg_obj,
                      msgcnt=mss_cnt),
        DirectoryAgent.address)
    mss_cnt += 1

    return gr


def directory_search_message(type):
    """
    Busca en el servicio de registro mandando un
    mensaje de request con una accion Seach del servicio de directorio

    Podria ser mas adecuado mandar un query-ref y una descripcion de registo
    con variables

    :param type:
    :return:
    """
    global mss_cnt
    logger.info('Buscamos en el servicio de registro')

    gmess = Graph()

    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[AgenteGenerador.name + '-search']
    gmess.add((reg_obj, RDF.type, DSO.Search))
    gmess.add((reg_obj, DSO.AgentType, type))

    msg = build_message(gmess, perf=ACL.request,
                        sender=AgenteGenerador.uri,
                        receiver=DirectoryAgent.uri,
                        content=reg_obj,
                        msgcnt=mss_cnt)
    gr = send_message(msg, DirectoryAgent.address)
    mss_cnt += 1
    logger.info('Recibimos informacion del agente')

    return gr


def search_agent(type):
    # Buscamos en el directorio un agente de hoteles
    gr = directory_search_message(type)

    # Obtenemos la direccion del agente de la respuesta
    # No hacemos ninguna comprobacion sobre si es un mensaje valido
    msg = gr.value(predicate=RDF.type, object=ACL.FipaAclMessage)
    content = gr.value(subject=msg, predicate=ACL.content)
    ragn_addr = gr.value(subject=content, predicate=DSO.Address)
    ragn_uri = gr.value(subject=content, predicate=DSO.Uri)

    return ragn_addr, ragn_uri


def infoagent_search(addr, ragn_uri, action, gm):
    """
    Envia una accion a un agente de informacion
    """
    global mss_cnt
    logger.info('Hacemos una peticion al servicio de Busqueda')

    gmess = Graph()

    gmess.bind('foaf', FOAF)
    gmess.bind('iaa', IAA)
    # Creacion de concepto SolicitarBusqueda
    reg_obj = DSO.SolicitarBusqueda
    gmess.add((reg_obj, DSO.Utiliza, DSO.InfoUsuario))
    for s, p, o in gm.triples((DSO.InfoUsuario, None, None)):
        gmess.add((s, p, o))

    gmess.add((reg_obj, RDF.type, action))

    msg = build_message(gmess, perf=ACL.request,
                        sender=AgenteGenerador.uri,
                        receiver=ragn_uri,
                        msgcnt=mss_cnt,
                        content=reg_obj)
    gr = send_message(msg, addr)
    logger.info('Recibimos respuesta a la peticion al servicio de informacion')
    mss_cnt += 1

    return gr


def process_transport_response(gmess, gr, reg_obj):
    # De moment nomes retorno el primer itinerario

    '''
    #Buscar el primer itinarario de vuelto
    busquedaVuelos = gr.value(predicate=RDF.type, object=DSO.ContenedorItinerarios)
    contenedorItinerarios = Collection(gr, busquedaVuelos)
    itinerario = contenedorItinerarios[0]
    '''
    # Buscar el itinerario mas barato
    qres = gr.query(
        """
        PREFIX ns:    <http://www.semanticweb.org/directory-service-ontology#>
        SELECT ?s
        WHERE { ?s ns:Precio ?Precio }
        ORDER BY ASC(xsd:integer(?Precio)) LIMIT 1
        """
    )
    if not qres or not qres.result:
        return
    itinerario = qres.result[0][0]
    gmess.add((reg_obj, DSO.Itinerario, itinerario))

    for s, p, o in gr.triples((itinerario, None, None)):
        gmess.add((s, p, o))  # VueloIda, VueloVuelta, Precio
        for s1, p1, o1 in gr.triples((o, None, None)):
            gmess.add((s1, p1, o1))  # Atributos vuelo


def process_hotel_response(gmess, gr, reg_obj):
    # De moment nomes retorno el primer hotel

    '''
    #Buscar el primer itinarario de vuelto
    busquedaHoteles = gr.value(predicate=RDF.type, object=DSO.ContenedorHoteles)
    contenedorItinerarios = Collection(gr, busquedaVuelos)
    itinerario = contenedorItinerarios[0]
    '''
    # Buscar el hotel mas barato
    qres = gr.query(
        """
        PREFIX ns:    <http://www.semanticweb.org/directory-service-ontology#>
        SELECT ?s
        WHERE { ?s ns:Precio ?Precio }
        ORDER BY ASC(xsd:integer(?Precio)) LIMIT 1
        """
    )
    if not qres or not qres.result:
        return
    hotel = qres.result[0][0]
    gmess.add((reg_obj, DSO.Hotel, hotel))

    for s, p, o in gr.triples((hotel, None, None)):
        gmess.add((s, p, o))


def process_activities_response(gmess, gr, reg_obj):
    busquedaActividades = gr.value(predicate=RDF.type, object=DSO.ContenedorActividades)
    contenedorActividadesResponse = Collection(gr, busquedaActividades)

    actividades = DSO.Actividades
    gmess.add((reg_obj, DSO.Actividades, actividades))
    gmess.add((actividades, RDF.type, DSO.ContenedorActividades))
    contenedorActividades = Collection(gmess, actividades)
    for act in contenedorActividadesResponse:
        contenedorActividades.append(act)
        for s, p, o in gr.triples((act, None, None)):
            gmess.add((s, p, o))


def process_transport(addr, ragn_uri, gm, gmess, reg_obj, return_dict):
    respuestaTransporte = infoagent_search(addr, ragn_uri, IAA.BuscarTransporte, gm)
    return_dict['transport'] = respuestaTransporte
    # process_transport_response(gmess, respuestaTransporte, reg_obj)


def process_activities(addr, ragn_uri, gm, gmess, reg_obj, return_dict):
    respuestaActividades = infoagent_search(addr, ragn_uri, IAA.BuscarActividades, gm)
    return_dict['activities'] = respuestaActividades


def process_hotel(addr, ragn_uri, gm, gmess, reg_obj, return_dict):
    respuestaAlojamiento = infoagent_search(addr, ragn_uri, IAA.BuscarAlojamiento, gm)
    return_dict['hotel'] = respuestaAlojamiento

@app.route("/iface", methods=['GET', 'POST'])
def browser_iface():
    """
    Permite la comunicacion con el agente via un navegador
    via un formulario
    """
    return 'Nothing to see here'


@app.route("/Stop")
def stop():
    """
    Entrypoint que para el agente

    :return:
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"


@app.route("/comm")
def comunicacion():
    """
    Entrypoint de comunicacion del agente
    Simplemente retorna un objeto fijo que representa una
    respuesta a una busqueda de vuelo

    Asumimos que se reciben siempre acciones que se refieren a lo que puede hacer
    el agente (buscar con ciertas restricciones, reservar)
    Las acciones se mandan siempre con un Request
    Prodriamos resolver las busquedas usando una performativa de Query-ref

    """

    def process_search_parallel():
        '''
        Para hacer las peticiones en paralelo, 
        proceso1 sera la busqueda de transporte por ejemplo y 
        le pasaremos el grafo respuesta como argumento.
        proceso1 es el nombre de una función.
        proceso1 hara la peticion al AgenteBuscador, recibira la respuesta,
        la procesara y añadira al grafo lo necesario para crear el plan.

        p1 = Process(target=process_transport, args=(gmess,))
        p1.start()
        p2 = Process(target=proceso2, args=(arr,))
        p2.start()
        p1.join()
        p2.join()
        :return: 
        '''

        global NUM_VIAJE
        # CREAMOS EL GRAFO DE RESPUESTA
        gmess = Graph()
        gmess.bind('dso', DSO)
        # Creacion de concepto PlanViaje
        reg_obj = DSO["PlanViaje" + str(NUM_VIAJE)]
        NUM_VIAJE += 1
        gmess.add((reg_obj, RDF.type, DSO.PlanViaje))

        logger.info("Buscamos el Agente Buscador en el Directorio de Servicios")
        addr, ragn_uri = search_agent(DSO.SearchAgent)
        # TODO: Hacerlo en paralelo
        manager = Manager()
        return_dict = manager.dict()
        jobs = []

        logger.info("Le pedimos al Agente Buscador una busqueda de Transporte")
        p1 = Process(target=process_transport, args=(addr, ragn_uri, gm, gmess, reg_obj, return_dict))
        jobs.append(p1)
        p1.start()

        logger.info("Le pedimos al Agente Buscador una busqueda de Actividades")
        p2 = Process(target=process_activities, args=(addr, ragn_uri, gm, gmess, reg_obj, return_dict))
        jobs.append(p2)
        p2.start()

        logger.info("Le pedimos al Agente Buscador una busqueda de Alojamiento")
        p3 = Process(target=process_hotel, args=(addr, ragn_uri, gm, gmess, reg_obj, return_dict))
        jobs.append(p3)
        p3.start()

        for j in jobs:
            j.join()

        process_transport_response(gmess, return_dict['transport'], reg_obj)
        process_activities_response(gmess, return_dict['activities'], reg_obj)
        process_hotel_response(gmess, return_dict['hotel'], reg_obj)

        data = gmess.serialize(format='xml')

        try:
            plan_id = planCollection.insert({"data": data})
            logger.error(plan_id)
            gmess.add((reg_obj, DSO.NumViaje, Literal(plan_id)))
        except:
            pass

        return gmess

    def process_search():

        global NUM_VIAJE
        # CREAMOS EL GRAFO DE RESPUESTA
        gmess = Graph()
        gmess.bind('dso', DSO)
        # Creacion de concepto PlanViaje
        reg_obj = DSO["PlanViaje" + str(NUM_VIAJE)]
        NUM_VIAJE += 1
        gmess.add((reg_obj, RDF.type, DSO.PlanViaje))
        gmess.add((reg_obj, DSO.NumViaje, Literal(NUM_VIAJE)))

        logger.info("Buscamos el Agente Buscador en el Directorio de Servicios")
        addr, ragn_uri = search_agent(DSO.SearchAgent)

        #TODO: Hacerlo en paralelo
        logger.info("Le pedimos al Agente Buscador una busqueda de Transporte")
        respuestaTransporte = infoagent_search(addr, ragn_uri, IAA.BuscarTransporte, gm)
        process_transport_response(gmess, respuestaTransporte, reg_obj)

        logger.info("Le pedimos al Agente Buscador una busqueda de Actividades")
        respuestaActividades = infoagent_search(addr, ragn_uri, IAA.BuscarActividades, gm)
        process_activities_response(gmess, respuestaActividades, reg_obj)

        logger.info("Le pedimos al Agente Buscador una busqueda de Alojamiento")
        respuestaAlojamiento = infoagent_search(addr, ragn_uri, IAA.BuscarAlojamiento, gm)
        process_hotel_response(gmess, respuestaAlojamiento, reg_obj)

        return gmess

    global dsgraph
    global mss_cnt

    logger.info('Peticion de informacion recibida')

    # Extraemos el mensaje y creamos un grafo con el
    message = request.args['content']
    gm = Graph()
    gm.parse(data=message)

    msgdic = get_message_properties(gm)

    # Comprobamos que sea un mensaje FIPA ACL
    if msgdic is None:
        # Si no es, respondemos que no hemos entendido el mensaje
        gr = build_message(Graph(), ACL['not-understood'], sender=AgenteGenerador.uri, msgcnt=mss_cnt)
    else:
        # Obtenemos la performativa
        perf = msgdic['performative']

        if perf != ACL.request:
            # Si no es un request, respondemos que no hemos entendido el mensaje
            gr = build_message(Graph(), ACL['not-understood'], sender=AgenteGenerador.uri, msgcnt=mss_cnt)
        else:
            # Extraemos el objeto del contenido que ha de ser una accion de la ontologia de acciones del agente
            # de registro

            # Averiguamos el tipo de la accion
            accion = None
            if 'content' in msgdic:
                content = msgdic['content']
                accion = gm.value(subject=content, predicate=RDF.type)
            # IAA = Namespace('IAActions')
            # Aqui realizariamos lo que pide la accion
            if accion == IAA.Search:
                if PARALLEL:
                    gr = process_search_parallel()
                else:
                    gr = process_search()
            # No habia ninguna accion en el mensaje
            else:
                gr = build_message(Graph(),
                                   ACL['not-understood'],
                                   sender=AgenteGenerador.uri,
                                   msgcnt=mss_cnt)

    mss_cnt += 1

    logger.info('Respondemos a la peticion')

    return gr.serialize(format='xml')


def tidyup():
    """
    Acciones previas a parar el agente

    """
    global cola1
    cola1.put(0)


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    # Registramos el agente
    gr = register_message()

    # Escuchando la cola hasta que llegue un 0
    fin = False
    while not fin:
        while cola.empty():
            pass
        v = cola.get()
        if v == 0:
            fin = True
        else:
            print (v)

            # Selfdestruct
            # requests.get(InfoAgent.stop)


if __name__ == '__main__':
    # Ponemos en marcha los behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Ponemos en marcha el servidor
    app.run(host=hostname, port=port)

    # Esperamos a que acaben los behaviors
    ab1.join()
    logger.info('The End')
