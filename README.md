# Documentación Profe 
* [Enunciado practica](http://www.lsi.upc.edu/~bejar/ecsdi/Laboratorio/ECSDI16172q.pdf)
* [Documentación LAB](http://www.lsi.upc.edu/~bejar/ecsdi/Laboratorio/ECSDILab16172q.pdf)
* [Transparencias RDF Collections](http://www.lsi.upc.edu/~bejar/ecsdi/Teoria/ECSDI05c-Lenguajes.pdf)
* [Exemple del profe](https://github.com/bejar/ECSDI2017/blob/master/Examples/RDFLib/RDFlib.ipynb)

# RDF
* [API doc rdflib Collection](http://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.collection.Collection)
* [Graph query examples](http://rdflib.readthedocs.io/en/stable/intro_to_graphs.html)
* [Example of making an RDF "Bag" with rdflib in Python](https://gist.github.com/nutjob4life/7344503)

# Multiprocessing
* [Exemple dictionary](https://stackoverflow.com/questions/10415028/how-can-i-recover-the-return-value-of-a-function-passed-to-multiprocessing-proce)
* [Doc oficial](https://docs.python.org/2/library/multiprocessing.html)