from rdflib.namespace import RDF, RDFS, Namespace, FOAF, OWL
from rdflib import Graph, BNode, Literal
from rdflib.collection import Collection


g = Graph()

DSO = Namespace('http://ejemplo.org/')
g.bind('ejemplo', DSO)


Vuelos = DSO.Vuelos
Vuelo = DSO.Vuelo
Vuelo1 = DSO.Vuelo1
Vuelo2 = DSO.Vuelo2
Vuelo3 = DSO.Vuelo3

g.add((Vuelos, RDF.type, DSO.ContenedorVuelos))
c = Collection(g, Vuelos)
c.append(Vuelo1)
c.append(Vuelo2)
g.add((Vuelo1, RDF.type, Vuelo))
g.add((Vuelo1, DSO.Num, Literal('ABC')))
g.add((Vuelo2, RDF.type, Vuelo))
g.add((Vuelo2, DSO.Num, Literal('CBA')))
#print ([term for term in c])
#print (g.serialize(format='xml'))


##SIMULEM QUE AGAFEM LA RESPOSTA
response = g.serialize(format='xml')
gr = Graph()
gr.parse(data=response)
vuelos = gr.value(predicate=RDF.type, object=DSO.ContenedorVuelos)
cr = Collection(gr, vuelos)
for vuelo in cr:
    num = gr.value(subject=vuelo, predicate=DSO.Num)
    print num

#-----------------------------------------------------------------------------


g1 = Graph()

DSO = Namespace('http://ejemplo.org/')
g1.bind('ejemplo', DSO)


Hoteles = DSO.Hoteles
Hotel = DSO.Hotel
Hotel1 = DSO.Hotel1
Hotel2 = DSO.Hotel2
Hotel2 = DSO.Hotel3

g1.add((Hoteles, RDF.type, DSO.ContenedorHoteles))
c1 = Collection(g1, Hoteles)
c1.append(Hotel1)
c1.append(Hotel2)
g1.add((Hotel1, RDF.type, Hotel))
g1.add((Hotel1, DSO.Num, Literal('123')))
g1.add((Hotel2, RDF.type, Hotel))
g1.add((Hotel2, DSO.Num, Literal('321')))
#print ([term for term in c])
#print (g.serialize(format='xml'))

##SIMULEM QUE AGAFEM LA RESPOSTA
response = g1.serialize(format='xml')
gr1 = Graph()
gr1.parse(data=response)
hoteles = gr1.value(predicate=RDF.type, object=DSO.ContenedorHoteles)
cr1 = Collection(gr1, hoteles)
for hotel in cr1:
    
    num = gr1.value(subject=hotel, predicate=DSO.Num)
    print num




'''
#EXEMPLE QUE NO SERVEIX
Vuelo = n.Vuelo
Vuelo1 = n.Vuelo1
Vuelo2 = n.Vuelo2
g.add((Vuelo1, RDF.type, Vuelo))
g.add((Vuelo1, n.Nom, Literal('ABC')))
g.add((Vuelo2, RDF.type, Vuelo))
g.add((Vuelo2, n.Nom, Literal('CBA')))

res = g.triples((None, RDF.type, Vuelo))

for vuelo in g.subjects(RDF.type, Vuelo):
    print (g.value(vuelo, n.Nom))
'''

print "SIMULEM ITINERARIS"
g = Graph()

DSO = Namespace('http://ejemplo.org/')
g.bind('ejemplo', DSO)


Itinerarios = DSO.Itinerarios
Itinerario = DSO.Itinerario
Itinerario1 = BNode()
Itinerario2 = BNode()
Vuelo = DSO.Vuelo
Vuelo1 = DSO.Vuelo1
Vuelo2 = DSO.Vuelo2
Vuelo3 = DSO.Vuelo3
Vuelo4 = DSO.Vuelo4

g.add((Itinerarios, RDF.type, DSO.ContenedorItinerarios))
i = Collection(g, Itinerarios)
i.append(Itinerario1)
i.append(Itinerario2)
g.add((Itinerario1, RDF.type, DSO.Itinerario))
g.add((Itinerario1, DSO.VueloIda, Vuelo1))
g.add((Itinerario1, DSO.VueloVuelta, Vuelo2))
g.add((Itinerario1, DSO.Precio, Literal(100)))
g.add((Vuelo1, RDF.type, Vuelo))
g.add((Vuelo1, DSO.Num, Literal('ABC')))
g.add((Vuelo2, RDF.type, Vuelo))
g.add((Vuelo2, DSO.Num, Literal('CBA')))

g.add((Itinerario2, RDF.type, DSO.Itinerario))
g.add((Itinerario2, DSO.Precio, Literal(200)))

qres = g.query(
    """
    PREFIX ns:    <http://ejemplo.org/>
    SELECT ?s
    WHERE { ?s ns:Precio ?Precio }
    ORDER BY ASC(xsd:integer(?Precio)) LIMIT 1
    """
)

print qres.result[0][0]
for s, p in qres.result:
    print s

exit(0)
#print ([term for term in c])
#print (g.serialize(format='xml'))


##SIMULEM QUE AGAFEM LA RESPOSTA
response = g.serialize(format='xml')
gr = Graph()
gr.parse(data=response)
itinerarios = gr.value(predicate=RDF.type, object=DSO.ContenedorItinerarios)
cr = Collection(gr, itinerarios)
for it in cr:
    print it
    vuelo_ida = gr.value(subject=it, predicate=DSO.VueloIda)
    print vuelo_ida
    num = gr.value(subject=vuelo_ida, predicate=DSO.Num)
    print num


it1 = cr[0]
for s,p,o in gr.triples( (it1, None, None)):
    print "Inside"
    print s,p,o