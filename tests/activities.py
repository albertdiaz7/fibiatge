# -*- coding: utf-8 -*-
from StdSuites import null
from googleplaces import GooglePlaces, types, lang
from AgentUtil.APIKeys import GOOGLE_PLACES_KEY
import json

ciudad = "Barcelona"
pais = "Spain"
palabras_clave = "Bar"
radio = 20000

google_places = GooglePlaces(GOOGLE_PLACES_KEY)
#query_result = google_places.nearby_search(
#        location='London, England',
#        radius=20000, types=[types.TYPE_FOOD])

query_result = google_places.nearby_search(
    location=ciudad + ', ' + pais,
    types='art_gallery',
    radius=radio)

if query_result.has_attributions:
    print query_result.html_attributions
for place in query_result.places:
    place.get_details()
    print json.dumps(place.details.get("opening_hours"), ensure_ascii=False)
    #print place.name + " | " + place.
